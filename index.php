<?php

ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);

require_once 'interfaces/Passable.php';
require_once 'classes/Php.php';
require_once 'classes/Mysql.php';
require_once 'classes/Student.php';

use Board\PHP as PHP;
use Board\MySQL as MySQL;
use Student;

$php = new PHP;
$mysql = new MySQL;

$student = new Student($php);

$student->addGrade(7);
$student->addGrade(8);
$student->addGrade(6);
$student->addGrade(10);


echo $php->average($student);

$student1 = new Student($mysql);

$student1->addGrade(4);
$student1->addGrade(8);
$student1->addGrade(6);
$student1->addGrade(2);


echo $mysql->average($student1);