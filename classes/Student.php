<?php

class Student {
    public $board;
    public $grades = [];

    public function __construct(Passable $board)
    {
        $this->board = $board;
    }

    public function addGrade($grade)
    {
        if(count($this->grades) < 4){
            $this->grades[] = $grade;
        }
    }
}