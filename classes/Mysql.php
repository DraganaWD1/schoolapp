<?php

namespace Board;

use \Passable;
use \Student;

class MySQL implements Passable 
{
    public function average(Student $student)
    {
        if(max($student->grades) >= 8){
            return true;
        }

        return false;
    }
}